package com.jaspreet.myapplication.ui.loan

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.ui.base.BaseFragment
import kotlinx.android.synthetic.main.frg_loan.*
import javax.inject.Inject

class LoanFrg :BaseFragment<LoanViewModel>()  {

    @Inject lateinit var loanViewModel: LoanViewModel

    override fun getViewModel(): LoanViewModel = loanViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  =
            inflater.inflate(R.layout.frg_loan,container,false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_submit.setOnClickListener {
            submit()
        }
    }

    fun submit(){
        Toast.makeText(requireContext(),"Your Lead is generated" , Toast.LENGTH_SHORT).show()
        requireActivity().finish()
    }

}