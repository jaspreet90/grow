package com.jaspreet.myapplication.ui.home.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.repo.network.model.OfferModel

class OfferAdapter (private val myDataset: MutableList<OfferModel>) :
    RecyclerView.Adapter<OfferAdapter.MyViewHolder>() {


    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view){

        var offerDescription =  view.findViewById(R.id.tv_offer_desc) as TextView
        var ivOffer =  view.findViewById(R.id.iv_offer) as ImageView
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_offers, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.offerDescription.text = myDataset[position].decription
        holder.ivOffer.setImageResource(myDataset[position].icon)
    }

    override fun getItemCount() = myDataset.size
}