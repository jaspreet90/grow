package com.jaspreet.myapplication.ui.loan

import android.os.Bundle
import android.support.v4.app.Fragment
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.ui.base.BaseActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class LoanActivity : BaseActivity() , HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override  fun supportFragmentInjector(): AndroidInjector<Fragment>?  =   fragmentDispatchingAndroidInjector


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loan)
        setUp()
    }

    fun setUp(){
        addOrReplaceFrg(R.id.fl_loan_container,LoanFrg(),true,false)
    }
}