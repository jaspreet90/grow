package com.jaspreet.myapplication.ui.home.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jaspreet.myapplication.R

class CategoryAdapter  (private val myDataset: MutableList<String>) :
    RecyclerView.Adapter<CategoryAdapter.MyViewHolder>() {


    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view){

         var catName =  view.findViewById(R.id.tv_cat_name) as TextView

    }


     override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
         val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_category, parent, false)

        return MyViewHolder(view)
    }

     override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

         holder.catName.text = myDataset[position]
    }

     override fun getItemCount() = myDataset.size
}