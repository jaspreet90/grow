package com.jaspreet.myapplication.repo.db.user

import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class UserRepoImpl @Inject constructor(private val userDao: UserDao) : UserRepo {

    override fun insertUser(userTable: UserTable) =
         Completable.fromAction{ userDao.insertUser(userTable)}


    override fun getUser(mobileNumber: String) : Single<List<UserTable>>  =
      userDao.getUser(mobileNumber)


}