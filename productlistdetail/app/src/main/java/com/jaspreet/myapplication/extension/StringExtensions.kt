package com.jaspreet.myapplication.extension

import java.util.regex.Pattern

private val PHONE = Pattern.compile("^[6-9]\\d{9}$")


fun CharSequence.isValidPhone() = isNotBlank() && PHONE.matcher(this).matches() && length == 10

fun CharSequence.isValidName() = isNotBlank() && length >2