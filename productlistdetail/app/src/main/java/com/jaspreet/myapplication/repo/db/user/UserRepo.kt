package com.jaspreet.myapplication.repo.db.user

import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Completable
import io.reactivex.Single

interface UserRepo {


    @Insert
    fun insertUser(userTable: UserTable) : Completable

    @Query("SELECT MOBILE_NUMBER , NAME from user_table where MOBILE_NUMBER=:mobileNumber")
    fun getUser(mobileNumber:String) : Single<List<UserTable>>

}