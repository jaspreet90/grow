package com.jaspreet.myapplication.ui.loan

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoanViewModelFactory  @Inject constructor(
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoanViewModel::class.java)) {

            return LoanViewModel() as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}