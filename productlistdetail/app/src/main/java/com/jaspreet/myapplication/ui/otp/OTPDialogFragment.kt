package com.jaspreet.myapplication.ui.otp

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.jaspreet.myapplication.BuildConfig
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.util.AppConstant.OTP_TIMEOUT
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.plugins.RxJavaPlugins
import java.util.*
import java.util.concurrent.TimeUnit


open class OTPDialogFragment : DialogFragment() {



    private val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
    private var compositeDisposable: CompositeDisposable? = null
    private var callbacks: Callbacks? = null

    private lateinit var tvOtpSent: TextView
    private lateinit var btnResendOtp: Button
    private lateinit var etOtp1: EditText
    private lateinit var etOtp2: EditText
    private lateinit var etOtp3: EditText
    private lateinit var etOtp4: EditText
    private lateinit var etOtp5: EditText
    private lateinit var etOtp6: EditText
    private lateinit var tvTime: TextView
    private lateinit var otpLoadingFrame: ViewGroup
    private lateinit var btnOtpVerified: ImageButton



    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(DialogFragment.STYLE_NO_TITLE)
        return dialog
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.frg_otp, container, false)

        isCancelable = BuildConfig.DEBUG

        bindViews(view)
        compositeDisposable = CompositeDisposable()

        arguments?.getString(KEY_MOBILE)?.let {
            tvOtpSent.text = getString(R.string.otp_sent_to, it)
        }

        bindOtpFields()
        startCountdownTimer()
        disableResendButton()

        btnResendOtp.setOnClickListener {
            callbacks?.onResendClicked(
                    arguments?.getString(KEY_MOBILE)
            )
        }
        return view
    }

    private fun bindViews(view: View) {
        tvOtpSent = view.findViewById(R.id.tv_otp_sent)
        btnResendOtp = view.findViewById(R.id.btn_resend_otp)
        etOtp1 = view.findViewById(R.id.et_otp_1)
        etOtp2 = view.findViewById(R.id.et_otp_2)
        etOtp3 = view.findViewById(R.id.et_otp_3)
        etOtp4 = view.findViewById(R.id.et_otp_4)
        etOtp5 = view.findViewById(R.id.et_otp_5)
        etOtp6 = view.findViewById(R.id.et_otp_6)
        tvTime = view.findViewById(R.id.tv_time)
        otpLoadingFrame = view.findViewById(R.id.otp_loading_frame)
        btnOtpVerified = view.findViewById(R.id.btn_otp_verified)
    }

    fun setCallbacks(callbacks: Callbacks) {
        this.callbacks = callbacks
    }

    private fun bindOtpFields() {
        addTextWatcher(etOtp1, etOtp2)
        addTextWatcher(etOtp2, etOtp3)
        addTextWatcher(etOtp3, etOtp4)
        addTextWatcher(etOtp4, etOtp5)
        addTextWatcher(etOtp5, etOtp6)
        addTextWatcher(etOtp6, null)
    }

    private fun addTextWatcher(from: EditText?, to: EditText?) {
        if (from == null) {
            return
        }
        from.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (editable.length == 1) {
                    to?.requestFocus()
                    if (etOtp1.text.isNotEmpty() &&
                            etOtp2.text.isNotEmpty() &&
                            etOtp3.text.isNotEmpty() &&
                            etOtp4.text.isNotEmpty() &&
                            etOtp5.text.isNotEmpty() &&
                            etOtp6.text.isNotEmpty() &&
                            callbacks != null) {
                        callbacks?.confirmOtp(
                                etOtp1.text.toString(),
                                etOtp2.text.toString(),
                                etOtp3.text.toString(),
                                etOtp4.text.toString(),
                                etOtp5.text.toString(),
                                etOtp6.text.toString(),
                            arguments!!.getString(KEY_NAME)?:"",
                                arguments!!.getString(KEY_MOBILE)
                        )
                    }
                }
            }
        })
    }

    fun startCountdownTimer() {
        compositeDisposable?.add(
                Observable.interval(1, TimeUnit.SECONDS)
                        .take(OTP_TIMEOUT + 1)
                        .map { OTP_TIMEOUT - it }
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            calendar.timeInMillis = it * 1000
                            updateTime(calendar)
                            if (it == 0.toLong()) {
                                enableResendButton()
                            }
                        }) { RxJavaPlugins.onError(it) })
    }

    private fun enableResendButton() {
        btnResendOtp.isEnabled = true
    }

    private fun updateTime(calendar: Calendar) {
        tvTime.text = String.format("%1\$tM:%1\$tS", calendar)
    }

    fun disableResendButton() {
        btnResendOtp.isEnabled = false
    }



    override fun onDestroyView() {
        compositeDisposable?.dispose()
        super.onDestroyView()
    }

    override fun onCancel(dialog: DialogInterface?) {
        super.onCancel(dialog)
    }


    override fun onDestroy() {
        super.onDestroy()
        callbacks = null
    }

    interface Callbacks  {

        fun onResendClicked(mobile: String?)

        fun confirmOtp(s: String, s1: String, s2: String, s3: String, s4: String, s5: String,name:String,mobileNumber:String )

    }

    fun newInstance(phone: String,name: String): OTPDialogFragment {

        val args = Bundle()
        args.putString(KEY_MOBILE, phone)
        args.putString(KEY_NAME, name)
        val fragment = OTPDialogFragment()
        fragment.arguments = args
        return fragment
    }

    companion object {

        const val KEY_MOBILE = "KEY_MOBILE"
        const val KEY_NAME= "KEY_NAME"


    }
}
