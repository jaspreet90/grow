package com.jaspreet.myapplication.ui.home.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.ui.loan.LoanFrg
import com.jaspreet.myapplication.ui.base.BaseFragment
import com.jaspreet.myapplication.ui.home.HomeViewModel
import com.jaspreet.myapplication.ui.home.adapters.CategoryAdapter
import com.jaspreet.myapplication.ui.home.adapters.LeadAdapter
import com.jaspreet.myapplication.ui.home.adapters.OfferAdapter
import javax.inject.Inject
import kotlinx.android.synthetic.main.frg_home.*
import com.jaspreet.myapplication.util.RecyclerItemClickListener


class HomeFrg: BaseFragment<HomeViewModel>() {

    @Inject lateinit var homeViewModel: HomeViewModel

    override fun getViewModel(): HomeViewModel = homeViewModel
    lateinit var categoryAdapter: CategoryAdapter
    lateinit var leadAdapter: LeadAdapter
    lateinit var offerAdapter: OfferAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  =
            inflater.inflate(R.layout.frg_home, container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    private fun setUp(){

        setCategoryRecyclerView()
        setLeadRecyclerView()
        setOfferAdapter()
    }

    private fun setCategoryRecyclerView(){
        categoryAdapter = CategoryAdapter(getCategory())
        rv_category.adapter = categoryAdapter


        rv_category.addOnItemTouchListener(
            RecyclerItemClickListener(context, rv_category, object : RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    if (position==1)
                         (activity as HomeActivity).startLoanActivity()
                    else
                        Toast.makeText(requireContext(),"Only Personal Loan is Available as of Now , Please Click Personal Loan to generate Leads", Toast.LENGTH_SHORT).show()
                }

                override fun onLongItemClick(view: View, position: Int) {

                }
            })
        )

    }



    fun setLeadRecyclerView(){
        leadAdapter= LeadAdapter(getLeads())
        rv_lead.adapter = leadAdapter
    }
    fun setOfferAdapter(){
        offerAdapter = OfferAdapter(getOffers())
        rv_offers.adapter = offerAdapter

    }
}