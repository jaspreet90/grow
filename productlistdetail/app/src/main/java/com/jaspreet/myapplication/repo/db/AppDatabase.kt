package com.jaspreet.myapplication.repo.db


import android.app.Application
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import com.jaspreet.myapplication.repo.db.user.UserDao
import com.jaspreet.myapplication.repo.db.user.UserTable

@Database(entities = [ UserTable::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao() : UserDao

    companion object {

        private val DB_NAME = "AppDatabase.db"


        fun createInstance(application: Application): AppDatabase {
            return Room.databaseBuilder(application, AppDatabase::class.java, DB_NAME)
                .build()
        }

    }
}