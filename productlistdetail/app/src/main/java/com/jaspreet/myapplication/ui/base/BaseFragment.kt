package com.jaspreet.myapplication.ui.base

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.extension.longToast
import com.jaspreet.myapplication.repo.network.model.LeadModel
import com.jaspreet.myapplication.repo.network.model.OfferModel
import dagger.android.support.AndroidSupportInjection

abstract class BaseFragment<out VM : BaseViewModel> : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (hasInjector()) {
            AndroidSupportInjection.inject(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getViewModel()?.getInternalErrorObservable()?.observe(viewLifecycleOwner, Observer {
            it?.let {
                val message = if (it.second == null) getString(it.first) else getString(it.first, it.second)
                activity?.longToast(message)
            }
        })
    }

    fun getCategory() : MutableList<String>{
        var list = mutableListOf<String>()
        list.add("Credit \n Card")
        list.add("Personal \n Loan")
        list.add("Business \n Loan")
        list.add("Home \n Loan")
        list.add("Wheeler \n Loan")
        list.add("Stock \n Market")
        list.add("Mutual \n Fund")
        list.add("Term \n Insurance")
        list.add("Health \n Insurance")
        list.add("Wheeler \n Insurance")
        list.add("Retirement \n Plan")
        list.add("Child \n Plan")
        return list
    }

    fun getLeads()  : MutableList<LeadModel>{

        var list = mutableListOf<LeadModel>()
        list.add(LeadModel("Personal Loan","Jaspreet","Initiation"))
        list.add(LeadModel("Home Loan","Happy","Progress"))
        list.add(LeadModel("Wheeler Loan","Mini","Discussion"))
        list.add(LeadModel("Home Loan","Gagan","Initiation"))
        return list
    }

    fun getOffers() : MutableList<OfferModel>{

        var list = mutableListOf<OfferModel>()
        list.add(OfferModel("Health Insurance for 50+ Age People", R.drawable.ic_health))
        list.add(OfferModel("Stock Price Going Up Since Last 5 Days ",R.drawable.ic_stock))
        list.add(OfferModel("Return Investment for these Companies are Very High ",R.drawable.ic_fund))
        list.add(OfferModel("Get 20 Lakh with Just Rs.500/month ",R.drawable.ic_plus))
        return list

    }


    fun addOrReplaceFrg(containerID:Int, frg: Fragment, isAdd:Boolean, isAddToBackStack:Boolean){

        var transaction= childFragmentManager.beginTransaction()
        if (isAdd){
            transaction.add(containerID,frg)
        } else{
            transaction.replace(containerID,frg)
        }

        if (isAddToBackStack){
            transaction.addToBackStack(null)
        }

        transaction.commit()
    }
    open fun hasInjector() = true

    abstract fun getViewModel(): VM
}