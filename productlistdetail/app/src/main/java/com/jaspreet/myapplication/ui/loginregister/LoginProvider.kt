package com.jaspreet.myapplication.ui.celeb

import com.jaspreet.myapplication.ui.loginregister.view.LoginFrg
import com.jaspreet.myapplication.ui.loginregister.view.RegisterFrg
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginProvider {

    @ContributesAndroidInjector(modules = [(LoginModule::class)])
    abstract fun bindLoginFrg(): LoginFrg

    @ContributesAndroidInjector(modules = [(LoginModule::class)])
    abstract fun bindRegFrg(): RegisterFrg
}