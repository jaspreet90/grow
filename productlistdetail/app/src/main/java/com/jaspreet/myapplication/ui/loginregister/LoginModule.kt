package com.jaspreet.myapplication.ui.celeb

import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.myapplication.ui.loginregister.view.LoginRegisterActivity
import dagger.Module
import dagger.Provides

@Module
class LoginModule {

    @Provides
    fun provideViewModel(loginRegisterActivity: LoginRegisterActivity, loginViewModelFactory: LoginViewModelFactory ) = ViewModelProvider(loginRegisterActivity, loginViewModelFactory).get(
        LoginViewModel::class.java)
}
