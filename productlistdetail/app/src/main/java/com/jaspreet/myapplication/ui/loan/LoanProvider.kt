package com.jaspreet.myapplication.ui.loan

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoanProvider {

    @ContributesAndroidInjector(modules = [(LoanModule::class)])
    abstract fun bindLoanFrg(): LoanFrg
 
}