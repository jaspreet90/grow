package com.jaspreet.myapplication.ui

import android.app.Activity
import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import com.jaspreet.myapplication.di.component.ApplicationComponent
import com.jaspreet.myapplication.di.component.DaggerApplicationComponent
import com.jaspreet.myapplication.di.module.ApplicationModule
import com.jaspreet.myapplication.util.AppConstant.PRIMARY_CHANNEL_ID
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject

class MyApplication : Application() , HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        applicationComponent = DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .build()
        applicationComponent.inject(this)

        createNotificationChannel()
        setupDefaultRxJavaErrorHandler()

    }

    private fun setupDefaultRxJavaErrorHandler() {
        RxJavaPlugins.setErrorHandler { throwable ->
            throwable.printStackTrace()
        }
    }

    fun createNotificationChannel() {

       var  mNotifyManager =  getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val notificationChannel =
                NotificationChannel(PRIMARY_CHANNEL_ID, "OTP_CHANNEL", NotificationManager.IMPORTANCE_HIGH)

            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.enableVibration(true)
            notificationChannel.description = "OTP Channel to receive OTP for Login and Register"

            mNotifyManager?.createNotificationChannel(notificationChannel)
        }
    }

    override fun activityInjector() = activityInjector
}