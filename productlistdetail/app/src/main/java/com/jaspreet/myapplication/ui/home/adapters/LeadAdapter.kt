package com.jaspreet.myapplication.ui.home.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.repo.network.model.LeadModel

class LeadAdapter   (private val myDataset: MutableList<LeadModel>) :
    RecyclerView.Adapter<LeadAdapter.MyViewHolder>() {


    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view){

        var leadType =  view.findViewById(R.id.tv_lead_type) as TextView
        var leadName =  view.findViewById(R.id.tv_lead_name) as TextView
        var status =  view.findViewById(R.id.tv_status) as TextView

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_lead, parent, false)

        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.leadType.text = myDataset[position].type
        holder.leadName.text = myDataset[position].name
        holder.status.text = myDataset[position].status
    }

    override fun getItemCount() = myDataset.size
}