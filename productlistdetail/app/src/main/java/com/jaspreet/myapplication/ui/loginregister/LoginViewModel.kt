package com.jaspreet.myapplication.ui.celeb

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.google.gson.Gson
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.extension.isValidPhone
import com.jaspreet.myapplication.domain.ValidateMobileOtpUseCase
import com.jaspreet.myapplication.extension.isValidName
import com.jaspreet.myapplication.repo.network.model.RegisterResponse
import com.jaspreet.myapplication.ui.base.BaseViewModel
import com.jaspreet.myapplication.ui.otp.OTPDialogFragment
import com.jaspreet.myapplication.util.SingleLiveEvent


class LoginViewModel(private val validateMobileOtpUseCase: ValidateMobileOtpUseCase )
    : BaseViewModel() , OTPDialogFragment.Callbacks {


    val otpDialogLiveData = SingleLiveEvent<RegisterResponse>()
    val resendClickLiveData=SingleLiveEvent<Unit>()
    var mobileNumberValidMutableLiveData = SingleLiveEvent<Unit>()
    var registerMutableLiveData = SingleLiveEvent<Unit>()

    var isRegister=false

    fun doLogin(mobileNumber: String){
        isRegister=false
        processMobileNumber(mobileNumber)
    }
    fun doRegister(name:String, mobileNumber:String){
        isRegister=true
        if(!name.isValidName()){
            return postError(R.string.invalid_name)
        }
        //processMobileNumber(mobileNumber)

        if (!mobileNumber.isValidPhone())
             return postError(R.string.invalid_mobile_number)

            registerMutableLiveData.postValue(Unit)
    }
    private fun processMobileNumber(mobileNumber: String) {
        if (!mobileNumber.isValidPhone())
            return postError(R.string.invalid_mobile_number)
        else
            mobileNumberValidMutableLiveData.postValue(Unit)

    }
    override fun onResendClicked(mobile: String?) {
        resendClickLiveData.postValue(Unit)
    }

    override fun confirmOtp(s: String, s1: String, s2: String, s3: String, s4: String, s5: String,name:String, mobileNumber:String) {

        Log.e("==confirm otp mobile==","confirm hai g")
        Log.e("==confirm otp mobile==",mobileNumber)
        Log.e("==confirm otp name==",name)

        val otp=  s + s1 + s2 + s3 + s4 + s5
        if(otp=="123456") {
            var registerResponse:RegisterResponse

            if (isRegister) {


                addDisposable(validateMobileOtpUseCase
                    .insertUser(mobileNumber, name)
                    .subscribe({

                        registerResponse= RegisterResponse("Sucess",true)

                         otpDialogLiveData.postValue(registerResponse)
                        Log.d("==sucess insert=","sucess")
                    }, {
                        if (it.message!!.contains("1555")){
                            registerResponse=RegisterResponse("User Already Registered",false)

                            Log.d("==User Already Exist==", "User Already Exist")
                        } else {
                            registerResponse=RegisterResponse("DB Error",false)
                            Log.d("==error insert=", it.message)
                        }

                        otpDialogLiveData.postValue(registerResponse)
                    })
                )


            } else {



                addDisposable(validateMobileOtpUseCase
                    .getUser(mobileNumber!!)
                    .subscribe({

                        registerResponse = if (it.isEmpty()){
                            RegisterResponse("User Does Not Exist, Please Sign Up First",false)
                        }  else {
                            RegisterResponse("Sucess",true)
                        }

                        otpDialogLiveData.postValue(registerResponse)
                        Log.d("==sucess get=",Gson().toJson(it))
                    }, {

                        registerResponse= RegisterResponse("DB Error ",false)

                        otpDialogLiveData.postValue(registerResponse)
                        Log.d("==error get=",it.message)
                    })
                )
            }

        }
    }
}