package com.jaspreet.myapplication.ui.loginregister.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.ui.base.BaseActivity
import com.jaspreet.myapplication.ui.home.view.HomeActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class LoginRegisterActivity  : BaseActivity() , HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override  fun supportFragmentInjector(): AndroidInjector<Fragment>?  =   fragmentDispatchingAndroidInjector



    override
    fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_register)
        addOrReplaceFrg(R.id.fl_container, RegisterFrg(),false,false)

    }

    fun openHomeActivity(){

        startActivity(Intent(this@LoginRegisterActivity, HomeActivity::class.java))
        finish()
    }

}