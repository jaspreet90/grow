package com.jaspreet.myapplication.ui.home.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.ui.base.BaseActivity
import com.jaspreet.myapplication.ui.home.view.HomeFrg
import com.jaspreet.myapplication.ui.loan.LoanActivity
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class HomeActivity : BaseActivity() , HasSupportFragmentInjector {

    @Inject
    lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>


    override  fun supportFragmentInjector(): AndroidInjector<Fragment>?  =   fragmentDispatchingAndroidInjector



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        addOrReplaceFrg(R.id.fl_container, HomeFrg(),false,false)

    }

    fun startLoanActivity(){
        startActivity(Intent(this@HomeActivity , LoanActivity::class.java))
    }


}
