package com.jaspreet.myapplication.ui.loginregister.view

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jaspreet.myapplication.R
import com.jaspreet.myapplication.repo.network.model.RegisterResponse
import com.jaspreet.myapplication.ui.base.BaseFragment
import com.jaspreet.myapplication.ui.celeb.LoginViewModel
import com.jaspreet.myapplication.ui.otp.OTPDialogFragment
import com.jaspreet.myapplication.util.AppConstant
import kotlinx.android.synthetic.main.frg_register.*
import kotlinx.android.synthetic.main.frg_register.btn_submit
import javax.inject.Inject

class RegisterFrg : BaseFragment<LoginViewModel>() , OTPDialogFragment.Callbacks {

    @Inject lateinit var loginViewModel: LoginViewModel

    private var otpDialogFragment: OTPDialogFragment? = null

    override fun getViewModel(): LoginViewModel = loginViewModel

    var mobileNumber=""
    var name=""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?  = inflater.inflate(R.layout.frg_register,container,false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUp()
    }

    fun setUp(){
        bindViewModel()
        setClickListener()
    }

    private fun setClickListener(){
        btn_submit.setOnClickListener {

            mobileNumber = et_mobile_number.text.toString()
            name = et_name.text.toString()

            Log.e("==submit mobile=",mobileNumber)
            Log.e("==submit name=",name)

            loginViewModel.doRegister(name,mobileNumber)
        }

        tv_login.setOnClickListener {

            (activity as LoginRegisterActivity).addOrReplaceFrg(R.id.fl_container,
                LoginFrg(),false,false)
        }
    }



    fun bindViewModel(){
        loginViewModel.registerMutableLiveData.observe(this, Observer {
            sendNotification()
        })

        loginViewModel.resendClickLiveData.observe(this, Observer {
            resendNotification()
        })

        loginViewModel.otpDialogLiveData.observe(this, Observer {
            handleOTPDialog(it)
        })
    }

    fun sendNotification() {

        buildNotification()
        showOTPDialog()

    }

    fun resendNotification(){
        otpDialogFragment?.apply {
            startCountdownTimer()
            disableResendButton()
        }
        buildNotification()
    }

    fun buildNotification(){
        val mBuilder = NotificationCompat.Builder(requireContext(), AppConstant.PRIMARY_CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(getString(R.string.app_name))
            .setContentText(getString(R.string.otp_is))
            .setAutoCancel(true)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        val notificationManager = NotificationManagerCompat.from(requireContext())
        notificationManager.notify(AppConstant.NOTIFICATION_ID, mBuilder.build())
    }




    private fun handleOTPDialog(it: RegisterResponse?) {
        if (it?.status == false) {
            Toast.makeText(requireContext(),it.message, Toast.LENGTH_SHORT).show()
        } else registerSucess()
    }

    private fun registerSucess(){
        hideOTPDialog()
        (activity as LoginRegisterActivity).openHomeActivity()
    }

    private fun hideOTPDialog() {
        otpDialogFragment?.dismiss()
        otpDialogFragment = null
    }

    private fun showOTPDialog() {

        otpDialogFragment = OTPDialogFragment().newInstance(mobileNumber,name)
        otpDialogFragment?.setCallbacks(this)
        otpDialogFragment?.show(childFragmentManager,
            TAG
        )
    }

    override fun onResendClicked(mobile: String?) {
        loginViewModel.onResendClicked(mobile)
    }

    override fun confirmOtp(s: String, s1: String, s2: String, s3: String, s4: String, s5: String, name:String,mobileNumber:String) {
        loginViewModel.confirmOtp(s,s1,s2,s3,s4,s5,name,mobileNumber)
    }


    companion object{
        const val TAG = "RegisterFragment"
    }

}