package com.jaspreet.myapplication.repo.network.model

data class RegisterResponse(

    var message:String,
    val status:Boolean=false

)