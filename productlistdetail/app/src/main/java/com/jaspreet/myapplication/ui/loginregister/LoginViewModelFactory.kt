package com.jaspreet.myapplication.ui.celeb

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.myapplication.domain.ValidateMobileOtpUseCase
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LoginViewModelFactory  @Inject constructor(
    private val validateMobileOtpUseCase: ValidateMobileOtpUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {

            return LoginViewModel(validateMobileOtpUseCase) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }


}