package com.jaspreet.myapplication.di.module

import android.app.Application
import com.jaspreet.myapplication.repo.db.AppDatabase
import com.jaspreet.myapplication.repo.db.user.UserRepo
import com.jaspreet.myapplication.repo.db.user.UserRepoImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class DbModule {

    @Provides
    @Singleton
    internal fun provideAppDb(application: Application) = AppDatabase.createInstance(application)

    @Provides
    @Singleton
    internal fun provideUserDao(appDb: AppDatabase) = appDb.userDao()

    @Singleton
    @Provides
    internal fun userRepo(userRepoImpl: UserRepoImpl): UserRepo = userRepoImpl


}