package com.jaspreet.myapplication.repo.network.model

data class LeadModel (
        var type:String ="",
        var name:String="",
        var status:String=""
)