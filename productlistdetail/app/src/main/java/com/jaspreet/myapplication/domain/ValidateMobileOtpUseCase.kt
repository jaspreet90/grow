package com.jaspreet.myapplication.domain

import android.util.Log
import com.jaspreet.myapplication.repo.db.user.UserRepo
import com.jaspreet.myapplication.repo.db.user.UserTable
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class ValidateMobileOtpUseCase @Inject constructor( private val userRepo: UserRepo) {




    fun getUser(mobileNumber: String) =

        userRepo.getUser(mobileNumber)
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())


    fun insertUser(mobileNumber: String, name: String):Completable {


            Log.d("==mobile Number==", mobileNumber)
            Log.d("==name==", name)

            var userObj = UserTable(mobileNumber, name)

            return  userRepo.insertUser(userObj)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())

    }

}