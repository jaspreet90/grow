package com.jaspreet.myapplication.util

object AppConstant {
    const val OTP_TIMEOUT: Long = 1 * 10

    const val PRIMARY_CHANNEL_ID = "otp_notification_channel"

    const val NOTIFICATION_ID = 0
}