package com.jaspreet.myapplication.ui.home

import android.arch.lifecycle.ViewModelProvider
import com.jaspreet.myapplication.ui.home.view.HomeActivity
import dagger.Module
import dagger.Provides

@Module
class HomeModule {
    @Provides
    fun provideViewModel(homeActivity: HomeActivity, homeViewModelFactory: HomeViewModelFactory ) = ViewModelProvider(homeActivity, homeViewModelFactory).get(
        HomeViewModel::class.java)
}
