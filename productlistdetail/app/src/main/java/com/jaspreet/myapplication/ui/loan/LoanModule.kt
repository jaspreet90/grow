package com.jaspreet.myapplication.ui.loan

import android.arch.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides

@Module
class LoanModule {
    @Provides
    fun provideViewModel(loanActivity: LoanActivity, loanViewModelFactory: LoanViewModelFactory ) = ViewModelProvider(loanActivity, loanViewModelFactory).get(
        LoanViewModel::class.java)
}
