package com.jaspreet.myapplication.repo.db.user

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "USER_TABLE")
data class UserTable (

    @PrimaryKey
    @ColumnInfo(name="MOBILE_NUMBER")
    var mobileNumber: String,

    @ColumnInfo(name="NAME")
    var name: String? = null
)