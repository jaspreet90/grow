package com.jaspreet.myapplication.di.component


import com.jaspreet.myapplication.di.builder.ActivityBuilder
import com.jaspreet.myapplication.di.module.ApplicationModule
import com.jaspreet.myapplication.ui.MyApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidInjectionModule::class), (ApplicationModule::class), (ActivityBuilder::class) ])
interface ApplicationComponent {

    fun inject(app: MyApplication)

}