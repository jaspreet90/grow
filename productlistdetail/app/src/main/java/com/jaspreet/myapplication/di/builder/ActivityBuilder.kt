package com.jaspreet.myapplication.di.builder

 import com.jaspreet.myapplication.ui.celeb.LoginProvider
 import com.jaspreet.myapplication.ui.home.view.HomeActivity
 import com.jaspreet.myapplication.ui.home.HomeProvider
 import com.jaspreet.myapplication.ui.loan.LoanActivity
 import com.jaspreet.myapplication.ui.loan.LoanProvider
 import com.jaspreet.myapplication.ui.loginregister.view.LoginRegisterActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(LoginProvider::class)])
    abstract fun bindLoginRegister(): LoginRegisterActivity

    @ContributesAndroidInjector(modules = [(HomeProvider::class)])
    abstract fun bindHome() : HomeActivity

    @ContributesAndroidInjector(modules = [(LoanProvider::class)])
    abstract fun bindLoan() : LoanActivity

}