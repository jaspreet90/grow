package com.jaspreet.myapplication.ui.home

import com.jaspreet.myapplication.ui.home.view.HomeFrg
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeProvider {

    @ContributesAndroidInjector(modules = [(HomeModule::class)])
    abstract fun bindHomeFrg(): HomeFrg

}