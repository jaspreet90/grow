package com.jaspreet.myapplication.repo.db.user

import android.arch.persistence.room.*
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface UserDao {


    @Insert
    fun insertUser(userTable: UserTable)

    @Query("SELECT MOBILE_NUMBER , NAME from user_table where MOBILE_NUMBER=:mobileNumber")
    fun getUser(mobileNumber:String) : Single<List<UserTable>>

}